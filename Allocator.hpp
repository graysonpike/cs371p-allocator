// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {

    // Two allocators are never equal
    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;}
        friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}


    public:

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;


    private:

        char a[N];

        /**
         * O(1) in space
         * O(n) in time
         * Check that the array is filled with valid blocks,
         * and that no two consecutive blocks are free blocks.
         */
        bool valid () const {
            int uncounted_bytes = sizeof(a) - sizeof(int);
            int pos = 0;
            int prev_header_value = 0;
            while (uncounted_bytes > 0) {
                if (pos > 0) {
                    int footer_value = (*this)[pos - sizeof(int)];
                    if (prev_header_value != footer_value) {
                        return false;
                    }
                }
                int header_value = (*this)[pos];
                prev_header_value = header_value;
                if (header_value == 0) {
                    return false;
                }
                if (header_value < 0) {
                    header_value = -header_value;
                }
                uncounted_bytes -= header_value + (2 * sizeof(int));
                pos += header_value + 2 * sizeof(int);
            }
            return true;
        }

    public:

        class iterator {

            friend bool operator == (const iterator& lhs, const iterator& rhs) {
                return lhs._p == rhs._p;
            }
            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);
            }

            private:
                int* _p;

            public:

                iterator (int* p) {
                    _p = p;}

                int& operator * () const {
                    return *_p;
                }

                iterator& operator ++ () {
                    // Move pointer forward by the space of the block, header, and footer
                    int block_size = *_p;
                    if (block_size < 0) {
                        block_size = -block_size;
                    }
                    _p += (block_size / sizeof(int)) + 2;
                    return *this;
                }

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}

                iterator& operator -- () {
                    // Move pointer backward by the space of the block, header, and footer
                    _p--;
                    int block_size = *_p;
                    if (block_size < 0) {
                        block_size = -block_size;
                    }
                    _p -= (block_size / sizeof(int)) + 1;
                    return *this;
                }

                iterator operator -- (int) {
                    iterator x = *this;
                    --*this;
                    return x;}
        };

        class const_iterator {

            friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
                return lhs._p == rhs._p;
            }

            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs == rhs);
            }

            private:
                int* _p;

            public:

                const_iterator (int* p) {
                    _p = p;
                }

                const int& operator * () const {
                    return const_cast<int&>(*_p);
                }

                const_iterator& operator ++ () {
                    // Move pointer forward by the space of the block, header, and footer
                    int block_size = *_p;
                    if (block_size < 0) {
                        block_size = -block_size;
                    }
                    _p += (block_size / sizeof(int)) + 2;
                    return *this;
                }

                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++*this;
                    return x;
                }

                const_iterator& operator -- () {
                    // Move pointer backward by the space of the block, header, and footer
                    _p--;
                    int block_size = *_p;
                    if (block_size < 0) {
                        block_size = -block_size;
                    }
                    _p -= (block_size / sizeof(int)) + 1;
                    return *this;
                }

                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --*this;
                    return x;
                }
        };

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            int free_size = sizeof(a) - (2 * sizeof(int));
            (*this)[0] = free_size;
            (*this)[sizeof(int) + free_size] = free_size;
            assert(valid());
        }

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type n) {
            if (n == 0) {
                return (pointer)0;
            }
            if (n < 0) {
                throw std::bad_alloc();
            }
            n = n * sizeof(value_type);
            // Search for a large enough unused block
            int bytes_needed = n;
            int pos = 0;
            while (pos < (int)sizeof(a)) {
                int header_value = (*this)[pos];
                if (header_value >= bytes_needed) {
                    // If the unused block is large enough, we will use it
                    // If the block is too small to leave a new unused block behind,
                    // we will allocate the entire block
                    if (header_value - bytes_needed < (int)(sizeof(T) + (2 * sizeof(int)))) {
                        int actual_size = (*this)[pos];
                        (*this)[pos] = -actual_size;
                        (*this)[pos + actual_size + sizeof(int)] = -actual_size;
                    }
                    // If space remaining is enough for a new unused block
                    else {
                        // Allocated block header
                        (*this)[pos] = -n;
                        // Allocated block footer
                        (*this)[pos + n + sizeof(int)] = -n;
                        int unused_size = header_value - n - (2 * sizeof(int));
                        // New unused block header
                        (*this)[pos + n + (2 * sizeof(int))] = unused_size;
                        // New unused block footer
                        (*this)[pos + n + (3 * sizeof(int)) + unused_size] = unused_size;
                    }
                    assert(valid());
                    return reinterpret_cast<pointer>(&((*this)[pos + sizeof(int)]));
                }
                if (header_value > 0) {
                    pos += header_value + 2 * sizeof(int);    
                } else {
                    pos += (-header_value) + 2 * sizeof(int);
                }
            }
            // Failed to find an unused block large enough for n T's
            throw std::bad_alloc();
            return nullptr;
        }

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * <your documentation>
         */
        void deallocate (pointer p, size_type n) {
            if (reinterpret_cast<char*>(p) < a) {
                throw std::bad_alloc();
            }
            long pos = (long)p - (long)a - sizeof(int);
            int header_value = (*this)[pos];
            int total_free_space = -header_value;
            int new_header_pos = pos;
            int new_footer_pos = pos + sizeof(int) + (-header_value);
            // If not zero, look left for free block to merge
            if (pos != 0) {
                int left_footer_value = (*this)[pos - sizeof(int)];
                if (left_footer_value > 0) {
                    // Include left block
                    total_free_space += left_footer_value + 2 * sizeof(int);
                    new_header_pos = pos - left_footer_value - 2 * sizeof(int);
                }
            }
            // If footer not end, look right for free block to merge
            int last_footer_pos = sizeof(a) - sizeof(int);
            if (new_footer_pos != last_footer_pos) {
                int right_header_value = (*this)[new_footer_pos + sizeof(int)];
                if (right_header_value > 0) {
                    // Include right block
                    total_free_space += right_header_value + 2 * sizeof(int);
                    new_footer_pos += 2 * sizeof(int) + right_header_value;
                }
            }
            (*this)[new_header_pos] = total_free_space;
            (*this)[new_footer_pos] = total_free_space;
            assert(valid());
        }

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        // iterator begin () {
        //     return iterator(&((*this)[0]));
        // }

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator begin () const {
            return const_iterator( const_cast<int*>(&((*this)[0])));
        }

        /**
         * O(1) in space
         * O(1) in time
         */
        // iterator end () {
        //     return iterator((*this)[0]);
        // }

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator end () const {
            return const_iterator( const_cast<int*>(&((*this)[1000])));
        }
};

#endif // Allocator_h
