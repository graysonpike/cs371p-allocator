# CS371p: Object-Oriented Programming Allocator Repo

* Name: Grayson Pike

* EID: gep552

* GitLab ID: graysonpike

* HackerRank ID: graysonpike

* Git SHA: (most recent Git SHA, final change to your repo will be adding this value)

* GitLab Pipelines: https://gitlab.com/graysonpike/cs371p-allocator/pipelines

* Estimated completion time: 5

* Actual completion time: 15

* Comments: 338a449bc959480a410ec653d8d20e63621abbd6
