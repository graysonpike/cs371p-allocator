#include <iostream> // cin, cout
#include <vector>
#include <string>

#include "Allocator.hpp"


using namespace std;


int main () {

	int num_cases;
	cin >> num_cases;
	std::string str;
	getline(cin, str); // blank line 
	getline(cin, str); // blank line 

	for (int case_num = 0; case_num < num_cases; case_num ++) {
		my_allocator<double, 1000> alloc = my_allocator<double, 1000>();
		std::vector<double*> allocated_blocks = std::vector<double*>();
		getline(cin, str);
		while (str != "") {
			int inst;
			inst = stoi(str); 
			if (inst > 0) {
				// Allocate this amount
				allocated_blocks.push_back(reinterpret_cast<double*>(alloc.allocate(inst)));
			} else {
				// Deallocate first allocated block
				int index = -inst - 1;
				alloc.deallocate(allocated_blocks[index], 1);
				allocated_blocks.erase(allocated_blocks.begin() + index);
			}
			getline(cin, str);
		}
		my_allocator<double, 1000>::const_iterator begin = alloc.begin();
		my_allocator<double, 1000>::const_iterator end = alloc.end();
		while (begin != end) {
			std::cout << *begin;
			begin++;
			if (begin != end) {
				std::cout << " ";
			}
		}
		std::cout << std::endl;
	}

	



    return 0;
}
